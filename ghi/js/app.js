function createCard(title, description, pictureUrl, start_date, end_date, location) {
    return `
    <div class="col-4">
        <div class="shadow">
            <div class="card">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                <h5 class="card-title">${title}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location} </h6>
                <p class="card-text">${description}</p>
                    <div class="card-footer">
                    ${start_date} - ${end_date}
                    </div>
                </div>
            </div>
        </div>
    </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {

const url = 'http://localhost:8000/api/conferences/';
const row = document.querySelector('.container .row')
try {
  const response = await fetch(url);


  if (!response.ok) {
    throw new Error('Response not ok')

  } else {
    const data = await response.json();


    for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
    if (detailResponse.ok) {
        const details = await detailResponse.json();
        const title = details.conference.name;
        const description = details.conference.description;
        const pictureUrl = details.conference.location.picture_url;
        const start = details.conference.starts;
        const end = details.conference.ends;
        const start_date = new Date(start).toLocaleDateString()
        const end_date = new Date(end).toLocaleDateString()
        const location = details.conference.location.name
        const html = createCard(title, description, pictureUrl, start_date, end_date, location);

        row.innerHTML += html;
        console.log(details)



    }
    }
  }
} catch (e) {
    console.error('error', e);
}


});
