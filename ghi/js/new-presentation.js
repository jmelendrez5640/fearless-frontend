window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/'

    const response = await fetch(url);
    const selectTag = document.getElementById('conference')
    if (response.ok) {
        const data = await response.json();


        for (let conference of data.conferences) {
            const option = document.createElement('option')
            const hrefId = conference.href.split('/')
            const conferenceId = hrefId[hrefId.length - 2];
            option.value = conferenceId
            option.innerHTML = conference.name
            selectTag.appendChild(option)
        }

    }
    const formTag = document.getElementById('create-presentation-form')
    formTag.addEventListener('submit', async event => {
        event.preventDefault();

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData))

        const currentConferenceId = selectTag.value;


        const presUrl = `http://localhost:8000/api/conferences/${currentConferenceId}/presentations/`
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(presUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newPres = await response.json();
            console.log(newPres)
        }
    })
})
